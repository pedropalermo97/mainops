class User < ApplicationRecord
    has_secure_password
    mount_uploader :image, ImageUploader

    ## Relações
    ## Validações
    validates :name, :age, :gender, presence: true
    validates :name, uniqueness: true
    validates :password, :password_confirmation, length: {minimum: 6}, on: :create
    enum gender:{
        "Mulher":0,
        "Homem":1
    }
    
    # funções
    private
        def generate_random_token
            SecureRandom.alphanumeric(15)
        end


end
