class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :gender, :age, :image
end
