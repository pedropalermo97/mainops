class AuthController < ApplicationController
  
  def login
    @user = User.find_by(name: params[:user][:name])
    if @user.authenticate(params[:user][:password])
      token = JsonWebToken.encode({user_id: @user.id})
      render json: {token: token, user: @user}
    else
      render json: {error: "Não foi possivel fazer o login"}, status: 401
    end

  end

  def signup
    @user = User.new(user_params)
    if @user.save
        render json: @user, status: 201
    else
      render json: @user.errors, status: 422
    end
  end

  def user_params
    params.require(:user).permit(:name, 
      :age,
      :gender,
      :password,
      :password_confirmation)
  end
end
