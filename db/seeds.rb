# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require "faker"
5.times.each do
    @user = User.create(name: Faker::Name.name,  password: "123456", password_confirmation: "123456", gender: rand(0..1), age: rand(0..99) )
end

